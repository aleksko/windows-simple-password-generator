#Install pwdgen to Power Shell init

if ( ![System.IO.File]::Exists($PROFILE) ) {
  New-Item -Path $PROFILE -ItemType File -Force
}

$p_content=(Get-Content $PROFILE -Raw)

If ( !($p_content -like '*pwgen*') ) {
   cat .\pwgen.ps1 >> $PROFILE
   Write-Output("installed successfully! Restart this Power Shell session and enjoy it. :-)")

} else {
   Write-Output("Script 'pwgen' already installed!")
}